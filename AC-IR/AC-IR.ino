/* Copyright 2019 David Conran
*modified by Kris Occhipinti 2022 for remote wifi control 
*/

#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRac.h>
#include <IRutils.h>
#include <WiFiManager.h>
#include <ESP8266WebServer.h>

const uint16_t kIrLed = 4;  // The ESP GPIO pin to use that controls the IR LED.
IRac ac(kIrLed);  // Create a A/C object using GPIO to sending messages with.
ESP8266WebServer server(80);

void setup() {
  WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP
  Serial.begin(115200);
  delay(200);

  //WiFiManager, Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wm;

  bool res;
  res = wm.autoConnect("MinSplit");

  if(!res) {
      Serial.println("Failed to connect");
      // ESP.restart();
  } 
  else {
      //if you get here you have connected to the WiFi    
      Serial.println("connected...yeey :)");
  }
    
  // Set up what we want to send.
  // See state_t, opmode_t, fanspeed_t, swingv_t, & swingh_t in IRsend.h for
  // all the various options.
  ac.next.protocol = decode_type_t::GREE;  // Set a protocol to use.
  ac.next.model = 1;  // Some A/Cs have different models. Try just the first.
  ac.next.mode = stdAc::opmode_t::kCool;  // Run in cool mode initially.

  options();//set default options
  server.on("/", handleRoot);
  server.begin();
}

void handleRoot() {
  String msg = "";
  
  for (uint8_t i = 0; i < server.args(); i++) {
    String name = server.argName(i);
    String svalue = server.arg(i);
    msg += name + ": " + svalue + "\n";

    if ( name == "celsius" ){
      bool value = get_bool(server.arg(i));
      ac.next.celsius = value;
      Serial.println("celsius: " + String(value));
      
    }else if (name == "degrees" ){
      int value = server.arg(i).toInt();
      ac.next.degrees = value; 
      Serial.println("Degrees: " + String(ac.next.degrees) +"F");
      
    }else if ( name == "light" ){
      bool value = get_bool(server.arg(i));
      ac.next.light = value;
      Serial.println(String(name) + String(value));
      
    }else if ( name == "beep" ){
      bool value = get_bool(server.arg(i));
      ac.next.beep = value;
      Serial.println(String(name) + String(value));
      
    }else if ( name == "econo" ){
      bool value = get_bool(server.arg(i));
      ac.next.econo = value;
      Serial.println(String(name) + String(value));
      
    }else if ( name == "filter" ){
      bool value = get_bool(server.arg(i));
      ac.next.filter = value;
      Serial.println(String(name) + String(value));
      
    }else if ( name == "turbo" ){
      bool value = get_bool(server.arg(i));
      ac.next.turbo = value;
      Serial.println(String(name) + String(value));
      
    }else if ( name == "quiet" ){
      bool value = get_bool(server.arg(i));
      ac.next.quiet = value;
      Serial.println(String(name) + String(value));
      
    }else if ( name == "clean" ){
      bool value = get_bool(server.arg(i));
      ac.next.clean = value;
      Serial.println(String(name) + String(value));
      
    }else if ( name == "power" ){
      bool value = get_bool(server.arg(i));
      ac.next.power = value;
      Serial.println(String(name) + String(value));
      
    }
    
    ac.next.fanspeed = stdAc::fanspeed_t::kMedium;  // Start the fan at medium.
    ac.next.swingv = stdAc::swingv_t::kOff;  // Don't swing the fan up or down.
    ac.next.swingh = stdAc::swingh_t::kOff;  // Don't swing the fan left or right.
    ac.next.sleep = -1;  // Don't set any sleep time or modes.
    ac.next.clock = -1;  // Don't set any current time if we can avoid it.
  
  }

  //int code = server.arg("code").toInt();

  //mySwitch.send(code, bits);
  ac.sendAc();
  msg += "\n\nCommand options are:\n\n      power, clean, quiet, turbo, filter, econo, beep, light, celsius, degrees\n\n";
  server.send(200, "text/plain", msg);

  Serial.println(msg);
}

bool get_bool(String value){
  if ( value == "true"){
   return 1;
  }else{
    return 0;
  }
 
}


void loop() {
  server.handleClient();

}



void options(){
  ac.next.celsius = false;  // Use Celsius for temp units. False = Fahrenheit
  ac.next.degrees = 78; 
  ac.next.fanspeed = stdAc::fanspeed_t::kMedium;  // Start the fan at medium.
  ac.next.swingv = stdAc::swingv_t::kOff;  // Don't swing the fan up or down.
  ac.next.swingh = stdAc::swingh_t::kOff;  // Don't swing the fan left or right.
  ac.next.light = true;  // Turn off any LED/Lights/Display that we can.
  ac.next.beep = false;  // Turn off any beep from the A/C if we can.
  ac.next.econo = false;  // Turn off any economy modes if we can.
  ac.next.filter = false;  // Turn off any Ion/Mold/Health filters if we can.
  ac.next.turbo = false;  // Don't use any turbo/powerful/etc modes.
  ac.next.quiet = false;  // Don't use any quiet/silent/etc modes.
  ac.next.sleep = -1;  // Don't set any sleep time or modes.
  ac.next.clean = false;  // Turn off any Cleaning options if we can.
  ac.next.clock = -1;  // Don't set any current time if we can avoid it.
  ac.next.power = true;  // Initially start with the unit on.
}
void set(){
  // For every protocol the library has ...


      ac.next.power = true;  // We want to turn on the A/C unit.
      Serial.println("Temp: " + String(ac.next.degrees) +"F");
      ac.sendAc();  // Have the IRac class create and send a message.
      delay(1000);  // Wait


  Serial.println("Starting from the begining again ...");
}
