extends Control

var buttons = preload("res://temp_button.tscn")
@onready var temp_buttons = $VBoxContainer/ScrollContainer/temp_buttons
@onready var temp_label = $"VBoxContainer/TEMP-Label"
@onready var output = $VBoxContainer/output

# Called when the node enters the scene tree for the first time.
func _ready():
	for t in range(78,65,-1):
		var button = buttons.instantiate()
		button.temp = str(t)
		temp_buttons.add_child(button)
		


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	temp_label.text = Global.temp + "℉"
	#output.text = Global.output_msg
	pass
