extends Node

var server = "192.168.1.19"
var power = "true"
var light = "true"
var temp = "78"

var output_msg = "ready"

@onready var http_request = HTTPRequest.new()


# Called when the node enters the scene tree for the first time.
func _ready():
	add_child(http_request)
	http_request.request_completed.connect(self._http_request_completed)



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func send():
	# Perform a GET request. The URL below returns JSON as of writing.
	var url = "http://" + server + "/?power=" + power + "&light=" + light + "&degrees=" + temp
	print(url)
	var error = http_request.request(url)
	output_msg = url
	if error != OK:
		push_error("An error occurred in the HTTP request.")
	
func _http_request_completed(result, response_code, headers, body):
	print(headers)
